const state = {
  funds: 10000,
  stocks: [],
};

const mutations = {
  buyStock: (state, {id, price, quantity}) => {
    const record = state.stocks.find(element => element.id === id);
    if (record) {
      record.quantity += quantity;
    } else {
      state.stocks.push({
        id: id,
        quantity: quantity
      })
    }
    state.funds -= price * quantity;
  },
  sellStock: (state, {id, price, quantity}) => {
    const record = state.stocks.find(element => element.id === id);
     console.log(record.quantity);
     console.log(quantity);
    // console.log(record.quantity > quantity);
    if (parseInt(record.quantity) > quantity) {
      record.quantity -= quantity;
    } else {
      state.stocks.splice(state.stocks.indexOf(record), 1);
    }
    state.funds += price * quantity;
  },
  setPortfolio: (state, portfolio) => {
    state.funds = portfolio.funds;
    state.stocks = portfolio.stockPortfolio ? portfolio.stockPortfolio : [];
  }
};

const actions = {
  sellStock({commit}, order) {
    commit('sellStock', order);
  }
};

const getters = {
  stockPortfolio(state, getters) {
    return state.stocks.map(stock => {
      const record = getters.stocks.find(element => element.id === stock.id);
      return {
        id: stock.id,
        quantity: stock.quantity,
        name: record.name,
        price: record.price,
      }
    });
  },
  funds(state) {
    return state.funds;
  }
};


export default {
  state,
  mutations,
  actions,
  getters
}
