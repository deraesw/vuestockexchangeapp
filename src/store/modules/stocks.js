import stocks from '../../data/stocks';

const state = {
  stocks: [],
};

const mutations = {
  setStocks(state, stocks) {
    state.stocks = stocks;
  },
  randomizedStocks(state) {
    state.stocks.forEach(stock => {
      let min = 0.9;
      let max = 1.1;
      let random = Math.random();
      let range = max - min;
      let adjustment = range * random;
      let result = min + adjustment;
      stock.price = Math.round(stock.price * result)
    });
  }
};

const actions = {
  buyStock: ({commit}, order) => {
    commit('buyStock', order);
  },
  initStocks: ({commit}) => {
    commit('setStocks', stocks);
  },
  randomizedStocks:({commit}) => {
    commit('randomizedStocks');
  }
};

const getters = {
  stocks: (state) => {
    return state.stocks;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
}
